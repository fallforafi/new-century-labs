<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;

class LabsLocations extends Model {
	
    protected $table = 'labs_locations';
}
