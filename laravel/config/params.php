<?php
return [
    'site_name' => 'New Century Labs',
    'order_email' => 'adel@newcenturylabs.com.',
    'from_email' => 'adel@newcenturylabs.com',
	'currency'=>[
            'USD'=>['symbol'=>'$','name'=>'US Dollors'],
    ],
    'currency_default'=>'USD',
    'languages'=>[
            'en_uk'=>'English (UK)',
            'en_us'=>'English (Us)',
    ],
    'language_default'=>'en_uk',
    'contentTypes'=>[
            'page'=>'Page',
            'email'=>'Email',
            'block'=>'Block',
    ],
    'order_prefix'=>"ncl",
    'password_pattern' => '(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}',
    'keywords' => 'test',
    'meta_description'=>'Traditionally to order a lab test you have to make an appointment to see your doctor, wait in clinic waiting room with other sick patients, pay your copay, pay the doctors fee and get a surprise bill in the mail, why would you want to go through the stress? Browse our test menu and order on demand, on your terms, it’s the 21st century.'
    
];
?>
