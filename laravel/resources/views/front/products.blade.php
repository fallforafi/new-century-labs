@extends('front')
<?php
$title = 'Test Menu';
$description = '';
$keywords = '';
?>
@include('front/common/meta')
@section('content')
<section class="bnr-area page-bnr-area bg-full bg-cntr valigner" style="background-image:url('{{ asset('front/images/testmenu2.jpg') }}');">
    <div class="container">
        <div class="bnr__cont valign white text-center col-sm-12 text-uppercase anime-flipInX">
            <h2>Test Menu</h2>
            <h4>INFORMATION ABOUT “YOU” WHEN YOU WANT IT!</h4>
        </div>
    </div>
</section>
<section class="test-bd-area pt50 pb50" >
    <div class="container">
        <p>Traditionally to order a lab test you have to make an appointment to see your doctor, wait in clinic waiting room with other sick patients, pay your copay, pay the doctors fee and get a surprise bill in the mail, why would you want to go through the stress?  Browse our test menu and order on demand, on your terms, it’s the 21st century.</p>
        <div class="test-menu-area table-responsive0">

            <h3>Test Menu</h3>

            <style>
                td[colspan] {
                    font-size: 28px;
                }
            </style>

            <table id="example" class="table table-area0 table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="col-sm-2">Quest Test Codes</th>
                        <th class="col-sm-8">Tests Names</th>
                        <!--th>Price</th-->
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Quest Test Codes</th>
                        <th>Name</th>
                        <!--th>Price</th-->
                    </tr>
                </tfoot>
                <tbody>
                    <?php
                    foreach ($products as $alphabet => $ps) {
                        ?>
                        <tr>
                            <td colspan="4"><?php echo $alphabet; ?></td>
                        </tr>
                        <?php
                        foreach ($ps as $product) {
                            ?>
                            <tr>
                                <td><?php echo $product->sku; ?></td>
                                <td><a href="<?php echo url('product/' . $product->key); ?>"><?php echo $product->name; ?></a>
                                    <a class="btn btn-view pul-rgt" href="<?php echo url('product/' . $product->key); ?>">View</a></td>
                                <!--td> @include('front/products/price') </td-->
                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</section>          


@endsection