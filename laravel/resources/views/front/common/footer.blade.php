<footer>

    <section class="ftr-area bg-gray white">
        <div class="container">

            <div class="ftr__box ftr__links col-sm-4 clrlist listview">
                <h4>Learn</h4>
                <ul>
                    <li><a href="{{url('how-to-order')}}">How to order</a></li>
                    <li><a href="{{url('shop')}}">Test Menu</a></li>
                    <li><a href="{{url('faq')}}">FAQ</a></li>
                </ul>
            </div>

            <div class="ftr__box ftr__links col-sm-4  clrlist listview">
                <h4>Company</h4>
                <ul>
                    <li><a href="{{url('about-us')}}">About Us</a></li>
                    <li><a href="{{url('blog')}}">Blog</a></li>
                    <li><a href="{{url('locations')}}">Locations</a></li>
                    <!--
<li><a href="{{url('contacts')}}">Contact Us</a></li>-->
                </ul>
            </div>


            <div class="ftr__box shake-icon col-sm-4 social-area icon-boxed icon-rounded clrlist listview ">
                <h4>Stay Connected</h4>
                <ul>
                    <li><a href="https://www.facebook.com/NewCenturyLabs/" target="_black"><i class="fa fa-facebook"></i> Like us on Facebook</a></li>
                    <li><a href="https://twitter.com/New_CenturyLabs" target="_black"><i class="fa fa-twitter"></i> Follow us on Twitter</a></li>
                    <li><a href="https://www.youtube.com/channel/UCT8xbfOLFiY6JeMaNCKTTag" target="_black"><i class="fa fa-youtube-play"></i> Subscribe our Youtube page</a></li>
                    <li><a href="https://plus.google.com/102097982760148436196" target="_black"><i class="fa fa-google-plus"></i> Follow us on Google+</a></li>
                </ul>
            </div>

        </div>
    </section>
    @include('front/common/copyright')
</footer>