@extends('front')

@section('content')


<section class="bnr-area page-bnr-area bg-full bg-cntr valigner" style="background-image:url('{{ asset('front/images/map-bnr.jpg') }}');">
    <div class="container">
        <div class="bnr__cont valign white text-center col-sm-12 text-uppercase anime-flipInX">
            <h2>LOCATIONS</h2>
            <h4></h4>
        </div>
    </div>
</section>
 

<section class="inr-intro-area">

    <section class="location-area pt30 pb30">
        <div class="container">
            <div class="map-area loc__fom col-sm-12 p0">
                @include('front/locations/search')
            </div>
            <div class="map-area loc__list col-sm-12 p0">
                
                        <?php
                        if (count($locations) > 0) {
                            ?>
                <table id="example" class="table table-area0 table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="clrhm p20"><h3>Locations</h3></th>
                        </tr>
                    </thead>
                    <tbody>
                            <?php
                            foreach ($locations as $location) {
                                    if(isset($location->distance) && $location->distance>=50){
                                            break;
                                    }
                                ?>
                                <tr>
                                    <td>
                                        <div id="BDV1" class="clrhm">
											<a tabindex="1" href="{{url('location')}}/<?php echo $location->id ?>"><i class="fa fa-map-marker fa-2x"></i><h3>
                                            <?php echo $location->name ?> <?php echo $location->city ?> <?php echo $location->address2 ?></h3>
                                            <span>
                                            <?php echo $location->address ?>, <?php echo $location->city ?>
                                            <?php echo $location->state ?>
                                            <?php echo $location->zipCode ?>
                                            <?php
                                            if(isset($location->distance)){
                                            ?>
                                            Distance : <?php echo round($location->distance,1) ?> mi,
                                            <?php //echo "Longitude : ".$location->longitude ?>,
                                            <?php //echo "Latitude : ".$location->latitude ?>
                                            <?php } ?>
											</span>
											</a>
                                        </div>
                                    </td>
                                </tr>

                                <?php
                            }
                            ?>
                                <tbody></table>
                                <?php
                        }
                        ?>
                    
                <!--
                <iframe src="https://www.google.com/maps/d/embed?mid=1JqtD_I0ZCwHSovJXCFHLwFBnQGI&hl=es&z=20&ll=" width="640" height="480"></iframe>
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d387145.86626760714!2d-74.25818793633874!3d40.70531105497054!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c24fa5d33f083b%3A0xc80b8f06e177fe62!2sNew+York%2C+NY!5e0!3m2!1sen!2s!4v1477480451017" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                -->
            </div>
            
        </div>
    </section>



</section>
<section class="box-area text-center pt20 pb50">
    <div class="container">

        <div class="box col-sm-4 anime-left ">
            <div class="box__img">
                <img src="{{ asset('front/images/pic.jpg') }}" alt="" />
            </div>
            <div class="box__cont ">
                <p>We make getting blood draws Easy and convenient for you</p>
            </div>				
        </div>


        <div class="box col-sm-4 anime-in">
            <div class="box__img">
                <img src="{{ asset('front/images/postBanner.png') }}" alt="" />
            </div>
            <div class="box__cont">
                <p>No appointments needed Walk-ins are welcome You are in and out!</p>
            </div>				
        </div>


        <div class="box col-sm-4 anime-right">
            <div class="box__img">
                <img src="{{ asset('front/images/latestPost2.png') }}" alt="" />
            </div>
            <div class="box__cont">
                <p>Access your results in 72 hours</p>
            </div>				
        </div>


    </div>
</section>


@endsection
