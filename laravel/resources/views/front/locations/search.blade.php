<form method="get" class="search-fom rds mb30" action="{{ url('locations') }}">
    <div class="form-group col-sm-3">
        <label for="search" >City</label>
        <input type="text" placeholder="City" class="form-control" name="city" id="city" autocomplete="off" value="<?php echo $search['city'] ?>">
    </div>
    <div class="form-group col-sm-3">
        <label for="search" >State</label>
        {!! Form::select('state', $states,$search['state'],['class' => 'form-control','name'=>'state']) !!}

    </div>
    <div class="form-group col-sm-3">
        <label for="search" >Zip</label>
        <input type="text" placeholder="zip" class="form-control" name="zip" id="zip" autocomplete="off" value="<?php echo $search['zip'] ?>">

    </div>
    <div class="form-group  col-sm-3">
        <input type="hidden" name="search" id="search" value="1">
        <button class="btn " type="submit" > <span>Search</span> <i class="fa fa-search fa-2x"></i></button> 
    </div>
</form> <div id="suggested_locations" style="display:none;" class="form-group has-feedback"></div>
<script>
    $('#q').keyup(function () {
        var q = $('#q').val();
        $.ajax({url: "{{url('locations/get/')}}",
            type: 'get',
            data: {
                q: q
            },
            success: function (result) {
                $("#suggested_locations").html(result);
            }
        });
    });

</script>